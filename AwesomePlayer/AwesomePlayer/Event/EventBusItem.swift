//
//  EventBusItem.swift
//  AwesomePlayer
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import Foundation

enum EventPriority: Comparable {
    case high
    case medium
    case low
}

protocol EventBusListenerProtocol: NSObject {
    func onEventOccur<Event: EventProtocol>(_ event: Event)
}

class EventBusItem: NSObject {
    let priority: EventPriority
    let listener: EventBusListenerProtocol
    
    init(priority: EventPriority,
         listener: EventBusListenerProtocol) {
        self.priority = priority
        self.listener = listener
    }
}
