//
//  EventBus.swift
//  AwesomePlayer
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import Foundation

class EventBus<Event: EventProtocol>: EventBusProtocol {
    
    // MARK: - Properties
    private var listeners = Dictionary<Event, NSHashTable<EventBusItem>>()
    
    private let eventPublisher: EventPublisherProtocol
    
    private let syncQueue = DispatchQueue(label: "com.pivanov.syncqueue",
                                          attributes: .concurrent)
    
    // MARK: - Lifecycle
    init(eventPublisher: EventPublisherProtocol = EventPublisher()) {
        self.eventPublisher = eventPublisher
    }
    
    // MARK: - EventBusProtocol
    func addListener(_ listener: EventBusListenerProtocol,
                     for event: Event,
                     with priority: EventPriority = .medium) {
        
        let eventBusItem = EventBusItem(priority: priority,
                                        listener: listener)
        
        syncQueue.async(flags: .barrier) {
            if let eventListeners = self.listeners[event] {
                guard !eventListeners.allObjects.contains(where: { $0 == eventBusItem }) else {
                    return
                }
                
                eventListeners.add(eventBusItem)
                
            } else {
                let items = NSHashTable<EventBusItem>.weakObjects()
                items.add(eventBusItem)
                self.listeners[event] = items
            }
        }
    }
    
    func addListener(_ listener: EventBusListenerProtocol,
                     for events: [Event],
                     with priority: EventPriority = .medium) {
        events.forEach {
            addListener(listener, for: $0, with: priority)
        }
    }
    
    func removeListener(_ listener: EventBusListenerProtocol, for event: Event) {
        syncQueue.async(flags: .barrier) {
            guard let eventListeners = self.listeners[event],
                  let listenerToRemove = eventListeners.allObjects.first(where: { $0.listener == listener }) else {
                return
            }
            
            eventListeners.remove(listenerToRemove)
        }
    }
    
    func removeListener(_ listener: EventBusListenerProtocol) {
        listeners.keys.forEach {
            removeListener(listener, for: $0)
        }
    }
    
    func publish(event: Event) {
        syncQueue.sync {
            guard let eventListeners = listeners[event] else {
                return
            }
            
            DispatchQueue.main.async {
                self.eventPublisher.publish(event: event,
                                            to: eventListeners.allObjects)
            }
        }
    }
}
