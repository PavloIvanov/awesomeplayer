//
//  EventPublisher.swift
//  AwesomePlayer
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import Foundation

class EventPublisher: EventPublisherProtocol {
    
    func publish<Event: EventProtocol>(event: Event,
                                       to item: EventBusItem) {
        item.listener.onEventOccur(event)
    }
    
    func publish<Event: EventProtocol>(event: Event,
                                       to items: [EventBusItem]) {
        items.sorted(by: { $0.priority > $1.priority }).forEach {
            publish(event: event, to: $0)
        }
    }
}
