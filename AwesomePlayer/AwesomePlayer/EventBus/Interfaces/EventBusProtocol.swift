//
//  EventBusProtocol.swift
//  AwesomePlayer
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import Foundation

protocol EventBusProtocol {
    
    associatedtype Event: EventProtocol
    
    func addListener(_ listener: EventBusListenerProtocol,
                     for event: Event,
                     with priority: EventPriority)
    func addListener(_ listener: EventBusListenerProtocol,
                     for events: [Event],
                     with priority: EventPriority
    )
    func removeListener(_ listener: EventBusListenerProtocol, for event: Event)
    func removeListener(_ listener: EventBusListenerProtocol)
    
    func publish(event: Event)
}
