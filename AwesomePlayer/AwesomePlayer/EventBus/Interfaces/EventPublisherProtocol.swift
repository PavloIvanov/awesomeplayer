//
//  EventPublisherProtocol.swift
//  AwesomePlayer
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import Foundation

protocol EventPublisherProtocol {
    func publish<Event: EventProtocol>(event: Event,
                                       to item: EventBusItem)
    func publish<Event: EventProtocol>(event: Event,
                                       to items: [EventBusItem])
}
