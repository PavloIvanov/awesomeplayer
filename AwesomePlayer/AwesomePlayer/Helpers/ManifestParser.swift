//
//  ManifestParser.swift
//  AwesomePlayer
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import Foundation

protocol ManifestParserProtocol {
    func parse(manifest: Int) -> Manifest
}

class ManifestParser: ManifestParserProtocol {
    func parse(manifest: Int) -> Manifest {
        return Manifest(duration: manifest)
    }
}
