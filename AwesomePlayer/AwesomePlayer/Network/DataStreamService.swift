//
//  DataStreamService.swift
//  AwesomePlayer
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import Foundation

protocol DataStreamServiceProtocol {
    func connect(with manifest: Manifest, completion: @escaping (Result<Int, Error>) -> ())
    func pause()
    func stop()
}

class DataStreamService: DataStreamServiceProtocol {
    
    var timer: Timer?
    var seconds: Int = 0
    
    func connect(with manifest: Manifest, completion: @escaping (Result<Int, Error>) -> ()) {
        timer = Timer(fire: Date(), interval: 1, repeats: true, block: { (timer) in
            self.seconds += 1
            DispatchQueue.main.async {
                completion(.success(self.seconds))
            }
        })
    }
    
    func pause() {
        timer?.invalidate()
    }
    
    func stop() {
        timer?.invalidate()
    }
}
