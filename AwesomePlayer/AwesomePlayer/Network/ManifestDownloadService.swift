//
//  ManifestDownloadService.swift
//  AwesomePlayer
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import Foundation

protocol ManifestDownloadServiceProtocol {
    func downloadManifest(completion: @escaping (Result<Int, Error>) -> ())
}

class ManifestDownloadService: ManifestDownloadServiceProtocol {
    
    func downloadManifest(completion: @escaping (Result<Int, Error>) -> ()) {
        DispatchQueue.main.async {
            completion(.success(25))
        }
    }
}
