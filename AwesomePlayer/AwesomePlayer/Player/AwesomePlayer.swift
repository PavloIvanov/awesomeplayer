//
//  AwesomePlayer.swift
//  AwesomePlayer
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import Foundation

class AwesomePlayer<Bus: EventBusProtocol>: PlayerProtocol {
    
    enum State {
        case none
        case playing
        case paused
        case ready
        case loaded
        case unload
        case error
    }
    
    private let eventBus: Bus
    private let manifestDownloadService: ManifestDownloadServiceProtocol
    private let manifestParser: ManifestParserProtocol
    private let dataStreamService: DataStreamServiceProtocol
    
    private var state: State = .none
    private var manifest: Manifest?
    private var currentSecond: Int = 0
    
    init(eventBus: Bus,
         manifestDownloadService: ManifestDownloadServiceProtocol,
         manifestParser: ManifestParserProtocol,
         dataStreamService: DataStreamServiceProtocol) {
        self.eventBus = eventBus
        self.manifestDownloadService = manifestDownloadService
        self.manifestParser = manifestParser
        self.dataStreamService = dataStreamService
    }
    
    func addListener() {
                
    }
    
    func removeListener() {
            
    }
    
    func load() {
        state = .loaded
        manifestDownloadService.downloadManifest { [weak self] (result) in
            switch result {
            case .success(let rawManifest):
                self?.manifest = self?.manifestParser.parse(manifest: rawManifest)
                self?.state = .ready
                
            case .failure(_):
                self?.state = .error
            }
        }
    }
    
    func unload() {
        dataStreamService.stop()
        state = .unload
    }
    
    func destroy() {
        state = .none
        manifest = nil
        currentSecond = 0
    }
    
    func play() {
        guard let manifest = manifest else {
            // FIXME: call onError
            return
        }
        
        dataStreamService.connect(with: manifest) { [weak self] (result) in
            switch result {
            case .success(let currentSecond):
                self?.currentSecond = currentSecond
                self?.state = .playing
                
            case .failure(_):
                // FIXME: call onError
                self?.state = .error
            }
        }
    }
    
    func pause() {
        dataStreamService.pause()
        state = .paused
    }
    
    func getDuration() -> Int {
        return manifest?.duration ?? 0
    }
    
    func getCurrentTime() -> Int {
        return currentSecond
    }
    
    func isPlaying() -> Bool {
        return state == .playing
    }
    
    func isPaused() -> Bool {
        return state == .paused
    }
    
    func isReady() -> Bool {
        return state == .ready
    }
    
    func isLoaded() -> Bool {
        return state == .loaded
    }
}
