//
//  PlayerProtocol.swift
//  AwesomePlayer
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import Foundation

protocol PlayerProtocol {
    
    func addListener()
    func removeListener()
    
    func load()
    func unload()
    func destroy()
    
    func play()
    func pause()
    
    func getDuration() -> Int
    func getCurrentTime() -> Int
    
    func isPlaying() -> Bool
    func isPaused() -> Bool
    func isReady() -> Bool
    func isLoaded() -> Bool
}
