//
//  PlayerEvent.swift
//  AwesomePlayer
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import Foundation

enum PlayerEvent: EventProtocol {
    case onLoaded
    case onUnloaded
    case onReady
    case onPlay
    case onPause
    case onTimeChanged
    case onPlaybackFinished
    case onError
}
