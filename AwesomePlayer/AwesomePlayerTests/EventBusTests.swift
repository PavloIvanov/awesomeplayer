//
//  EventBusTests.swift
//  AwesomePlayerTests
//
//  Created by Pavlo Ivanov on 14.04.2021.
//

import XCTest
@testable import AwesomePlayer

fileprivate enum MockEvent: EventProtocol {
    case play
    case pause
}

enum MockEvent2: EventProtocol {
    case ta
}

class EventBusTests: XCTestCase {
    
    func testRegister() {
        // Given
        let eventPublisherMock = EventPublisherMock()
        let eventBus = EventBus<MockEvent>(eventPublisher: eventPublisherMock)
        let listenerMock = ListenerMock()
        
        // When
        eventBus.addListener(listenerMock,
                             for: .play,
                             with: .medium)
    }
}

fileprivate class EventPublisherMock: EventPublisherProtocol {
    
    var event: MockEvent?
    var item: EventBusItem?
    var items: [EventBusItem]?
    
    func publish<MockEvent>(event: MockEvent, to item: EventBusItem) where MockEvent : EventProtocol {
//        self.event = event
        self.item = item
    }
    
    func publish<MockEvent>(event: MockEvent, to items: [EventBusItem]) where MockEvent : EventProtocol {
        self.items = items
    }
}

fileprivate class ListenerMock: NSObject, EventBusListenerProtocol {
        
    var event: MockEvent2?
    
    func onEventOccur<MockEvent2: EventProtocol>(_ event: MockEvent2) {
//        self.event = event
    }
}
